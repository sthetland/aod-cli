rem *** Hallo, I'm the Atlassian AOD CLI solution
rem *** This is the main FishEye bat file, containing pages to be copied to the AOD space. 

@echo off
rem remember the directory path to this bat file
set dirPath=%~dp0

rem need to reverse windows names to posix names by changing \ to /
set dirPath=%dirPath:\=/%
rem remove blank at end of string
set dirPath=%dirPath:~0,-1%

rem get server, username and password

set /p server= "Server: "
set /p username= "Username: "
set /p password= "Password: "

rem Start off the logging to a text file
echo Atlassian AOD CLI report in "FishEyeDocsLog.txt" > FishEyeDocsLog.txt
echo START OF REPORT >> FishEyeDocsLog.txt

@echo on

java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "Using the FishEye Screens" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Working with Source" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "Browsing through a Repository" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Using the FishEye Screens" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "Searching FishEye" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Using the FishEye Screens" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "Viewing a File" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Using the FishEye Screens" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "Viewing File Content" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Viewing a File" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "Using Side by Side Diff View" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Viewing File Content" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "Viewing a File History" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Viewing a File" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "Viewing the Changelog" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Viewing a File" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "Copying and Pasting Code from FishEye" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Viewing a File" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "FishEye Charts" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Using the FishEye Screens" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "Using Favourites in FishEye" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Using the FishEye Screens" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "Viewing the Commit Graph for a Repository" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Using the FishEye Screens" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "Viewing People's Statistics" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Using the FishEye Screens" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "Antglob Reference Guide" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Working with Source" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "Date Expressions Reference Guide" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Working with Source" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "EyeQL Reference Guide" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Working with Source" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "Using Smart Commits" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "Working with Source" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "__JIRA integration" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "__newreleaseFishEye" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_FishEye 101 Basics" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_FishEye 101 Advanced" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_FishEye 101 Install" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_fishEyeSearch" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_MajorReleaseNumberFishEye" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_IconsFishEye" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_ImagesFishEye" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_admLogin" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_repositoryOperations" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_branchTagSelector" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_storeDiffInfo" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_upgradeFishEyeNote" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_UpgradeFECRU" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_Internal_Repository_Notice" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_hostedFishEyeName" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_hostedPartialApplyNoteFE" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt
java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "FISHEYE027" --title "_hostedNotApplicableFE" --newSpace "AOD" --replace --copyAttachments --noConvert --parent "_FishEyeInclusions" >> FishEyeDocsLog.txt


echo END OF REPORT >> FishEyeDocsLog.txt
rem *** That's it, I've finished! The log is in "FishEyeDocsLog.txt"
rem *** Bye!
@echo off


pause
rem Exit with the correct error level.
EXIT /B %ERRORLEVEL%